Rails.application.routes.draw do
  root 'tutorials#index'
  resources :bookmarks
  resources :votes
  resources :comment_comments
  resources :comments
  resources :tutorial_categories
  resources :categories
  resources :tutorials
  resources :users
  resources :sessions, only: [:create, :new, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
