class CreateBookmarks < ActiveRecord::Migration[5.0]
  def change
    create_table :bookmarks do |t|
      t.references :user, foreign_key: true, null: false
      t.references :tutorial, foreign_key: true, null: false
      t.string :link, null: false

      t.timestamps null: false
    end
  end
end
