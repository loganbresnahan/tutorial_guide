class CreateCommentComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comment_comments do |t|
      t.references :comment, foreign_key: true
      t.integer :child_comment_id

      t.timestamps null: false
    end
  end
end
