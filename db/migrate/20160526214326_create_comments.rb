class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.string :body, null: false
      t.integer :user_id, null: false
      t.integer :parent_comment_id
      # t.integer :tut_commenter_id
      # t.integer :com_commenter_id
      t.references :tutorial, foreign_key: true, null: false

      t.timestamps null: false
    end
    add_index :comments, :user_id
    add_index :comments, :parent_comment_id
    # add_index :comments, :tut_commenter_id
    # add_index :comments, :com_commenter_id
  end
end
