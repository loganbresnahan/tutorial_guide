class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    create_table :votes do |t|
      t.integer :user_id, null: false
      t.integer :value, null: false
      # t.integer :tut_voter_id
      # t.integer :com_voter_id
      t.references :tutorial
      t.references :comment, foreign_key: true

      t.timestamps null: false
    end
    add_index :votes, :user_id
    # add_index :votes, :tut_voter_id
    # add_index :votes, :com_voter_id
  end
end
