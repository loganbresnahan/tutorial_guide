class CreateTutorials < ActiveRecord::Migration[5.0]
  def change
    create_table :tutorials do |t|
      t.string :title, null: false
      t.string :link, null: false
      t.string :thumbnail, null: false
      t.string :web_title, null: false
      t.string :body, null: false
      t.references :user, null: false

      t.timestamps null: false
    end
  end
end
