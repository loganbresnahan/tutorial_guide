class CreateTutorialCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :tutorial_categories do |t|
      t.references :tutorial, foreign_key: true, null: false
      t.references :category, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
