# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160601193253) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "user_id",     null: false
    t.integer  "tutorial_id", null: false
    t.string   "link",        null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["tutorial_id"], name: "index_bookmarks_on_tutorial_id", using: :btree
    t.index ["user_id"], name: "index_bookmarks_on_user_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comment_comments", force: :cascade do |t|
    t.integer  "comment_id"
    t.integer  "child_comment_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["comment_id"], name: "index_comment_comments_on_comment_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.string   "body",              null: false
    t.integer  "user_id",           null: false
    t.integer  "parent_comment_id"
    t.integer  "tutorial_id",       null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["parent_comment_id"], name: "index_comments_on_parent_comment_id", using: :btree
    t.index ["tutorial_id"], name: "index_comments_on_tutorial_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "tutorial_categories", force: :cascade do |t|
    t.integer  "tutorial_id", null: false
    t.integer  "category_id", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_tutorial_categories_on_category_id", using: :btree
    t.index ["tutorial_id"], name: "index_tutorial_categories_on_tutorial_id", using: :btree
  end

  create_table "tutorials", force: :cascade do |t|
    t.string   "title",      null: false
    t.string   "link",       null: false
    t.string   "thumbnail",  null: false
    t.string   "web_title",  null: false
    t.string   "body",       null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_tutorials_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",        null: false
    t.string   "email",           null: false
    t.string   "password_digest", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "user_id",     null: false
    t.integer  "value",       null: false
    t.integer  "tutorial_id"
    t.integer  "comment_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["comment_id"], name: "index_votes_on_comment_id", using: :btree
    t.index ["tutorial_id"], name: "index_votes_on_tutorial_id", using: :btree
    t.index ["user_id"], name: "index_votes_on_user_id", using: :btree
  end

  add_foreign_key "bookmarks", "tutorials"
  add_foreign_key "bookmarks", "users"
  add_foreign_key "comment_comments", "comments"
  add_foreign_key "comments", "tutorials"
  add_foreign_key "tutorial_categories", "categories"
  add_foreign_key "tutorial_categories", "tutorials"
  add_foreign_key "votes", "comments"
end
