$(document).on('turbolinks:load', function(){

  $('#header').on('click', '#login-link', function(event){
    event.preventDefault();
    $('#modal-login').css('display', 'block');
  });

  $('#login-close').on('click', function(){
    $('#modal-login').css('display', 'none');
  });
  
})
