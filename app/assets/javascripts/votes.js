$(document).on('turbolinks:load', function(){

  $('.vote-hand-up').on('click', function(){
    var vote_value = $(this).next().next();
    var tutorial_id = $(this).next().next().attr('id');
    var up_current_display = $(this);
    var up_display = $(this).next();
    var down_display = $(this).next().next().next();
    var down_display_current = $(this).next().next().next().next();
    setTimeout(function(){
      $.ajax({
        url: 'tutorials/' + tutorial_id,
        dataType: 'json',
        method: 'get',
        error: function () { "error" },
        success: function (data) {
          vote_value.text(data['votes_total']);
          up_current_display.css('display', 'none');
          up_display.css('display', 'inline');
          down_display.html('<a url="/votes.' + data["vote_id"] + '"' + ' rel="nofollow" data-method="delete" href="/votes/' + data["vote_id"] + '"' + '><i class="fa fa-hand-o-down" aria-hidden="true"></i></a>');
          down_display.css('display', 'inline');
          down_display_current.css('display', 'none');
        },
      })
    }, 50);
  });

  $('.vote-hand-down').on('click', function(){
    var vote_value = $(this).prev();
    var tutorial_id = $(this).prev().attr('id');
    var down_current_display = $(this);
    var down_display = $(this).next();
    var up_display = $(this).prev().prev().prev();
    var up_display_current = $(this).prev().prev();
    setTimeout(function(){
      $.ajax({
        url: 'tutorials/' + tutorial_id,
        dataType: 'json',
        method: 'get',
        error: function () { "error" },
        success: function (data) {
          vote_value.text(data['votes_total']);
          down_current_display.css('display', 'none');
          down_display.css('display', 'inline');
          up_display_current.css('display', 'none');
          up_display.css('display', 'inline');
        },
      })
    }, 50);
  });

})
