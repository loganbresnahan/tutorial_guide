class SessionsController < ApplicationController

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      redirect_back(fallback_location: nil)
    else
      #error
      render :new
    end
  end

  def new 
  end

  def destroy
    session.delete(:user_id)
    redirect_to root_url
  end

end
