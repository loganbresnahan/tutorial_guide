class VotesController < ApplicationController

  def create
    @vote = Vote.new
    @vote.user_id = current_user.id
    @vote.value = params[:value]
    @vote.tutorial_id = params[:tutorial_id]
    @vote.comment_id = params[:comment_id]
    # @vote.tutorial_id = session[:tutorial_id]
    # @vote.comment_id = session[:comment_id]
    if @vote.save
    else
      # error
    end
  end

  def new
  end

  def destroy
    begin
      # @vote.destroy
      Vote.find_by(id: params[:id]).destroy
    rescue StandardError
    end
  end

  private

  def vote_params
    params.require(:vote).permit(:user_id, :value, :tutorial_id, :comment_id)
  end

end
