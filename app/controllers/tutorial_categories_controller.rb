class TutorialCategoriesController < ApplicationController

  def create
    TutorialCategory.new(tutorial_category_params).save
  end

  def new
  end

  private
    def tutorial_category_params
      params.require(:tutorial_category).permit(:tutorial_id, :category_id)
    end

end
