class CommentsController < ApplicationController

  def create
    comment = Comment.new(comment_params)
    if comment.save
      redirect_to tutorial_path(comment.tutorial_id)
    else
      render :new
    end
  end

  def new
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def update
    comment = Comment.find(params[:id])
    if comment.update_attributes(comment_params)
      redirect_to tutorial_path(comment.tutorial_id)
    else
      render :edit
    end
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def destroy
    comment = Comment.find(params[:id])
    comment.destroy
    redirect_to tutorial_path(comment.tutorial_id)
  end
end
