class TutorialsController < ApplicationController

  def index
    @tutorials = Tutorial.all
  end

  def create
    @tutorial = Tutorial.new(tutorial_params)
    @tutorial.thumbnail = @tutorial.favicon_parse(tutorial_params[:link])
    @tutorial.web_title = @tutorial.title_parse(tutorial_params[:link])
    if @tutorial.save
      redirect_to tutorial_path(@tutorial)
    else
      render :new
    end
  end

  def new
  end

  def show
    @tutorial = Tutorial.find(params[:id])
    @votes_total = @tutorial.votes_total(@tutorial.votes)
    @vote = @tutorial.find_vote(@tutorial, current_user)
    @vote_id = @tutorial.find_vote_id(@tutorial, current_user)
    respond_to do |f|
      f.html
      f.json { render json: { votes_total: @votes_total, vote: @vote, vote_id: @vote_id } }
    end
  end

  def update
    tutorial = Tutorial.find(params[:id])
    tutorial.thumbnail = tutorial.favicon_parse(tutorial.link)
    tutorial.web_title = tutorial.title_parse(tutorial.link)
    if tutorial.update_attributes(tutorial_params)
      #redirect_to group page at the tutorial? or tutorial is on top for signed in user?
    else
      render :edit
    end
  end

  def edit
    @tutorial = Tutorial.find(params[:id])
    redirect_to user_path(session[:user_id]) unless current_user.id == @tutorial.user_id
  end

  def destroy
    Tutorial.find(params[:id]).destroy
    redirect_to user_path(current_user)
  end

  private

    def tutorial_params
      params.require(:tutorial).permit(:title, :link, :thumbnail, :body, :user_id)
    end

end
