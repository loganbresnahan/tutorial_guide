class BookmarksController < ApplicationController

  def index
    @bookmarks = Bookmark.all
  end

  def create
    bookmark = Bookmark.new(bookmark_params)
    if bookmark.save
      redirect_back(fallback_location: nil)
    else
      flash[:bookmark_create_error] = "Did not save"
    end
  end

  def new
  end

  def show
    @bookmark = Bookmark.find(params[:id])
  end

  def update
    bookmark = Bookmark.find(params[:id])
    if bookmark.update_attributes(bookmark_params)
      redirect_back(fallback_location: nil)
    else
      flash[:bookmark_update_error] = "Did not update"
    end
  end

  def edit
    @bookmark = Bookmark.find(params[:id])
  end

  def destroy
    Bookmark.find(params[:id]).destroy
    redirect_back(fallback_location: nil)
  end

  private

  def bookmark_params
    params.require(:bookmarks).permit(:user_id, :tutorial_id, :link)
  end

end
