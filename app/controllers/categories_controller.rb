class CategoriesController < ApplicationController

  def index
    @categories = Category.all
  end

  def create
    category = Category.new(category_params)
    if category.save
      redirect_to category_path(category)
    else
      render :new
    end
  end

  def show
    @category = Category.find(params[:id])
  end

  def update
    Category.find(params[:id]).update_attributes(category_params)
  end

  def destroy
    Category.find(params[:id]).destroy
  end

  private

    def category_params
      params.require(:category).permit(:name)
    end
end
