module CommentsHelper

  class CommentsPresenter
    attr_reader :all_comments

    def initialize
      @all_comments = []
    end

    def recursive_comments_display(recursed_comments)
      @all_comments << recursed_comments
    end
  end

end
