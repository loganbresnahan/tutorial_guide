module VotesHelper

  def find_vote(tutorial, current_user)
    vote_return = nil
    tutorial.votes.each do |vote|
      current_user.votes.each do |personal_vote|
        vote_return = vote if vote.id == personal_vote.id
      end
    end
    vote_return
  end

  def find_vote_id(tutorial, current_user)
    tutorial.votes.each do |vote|
      current_user.votes.each do |personal_vote|
        return personal_vote.id if vote.id == personal_vote.id
      end
    end
  end

  def does_user_vote_exist(vote)
    current_user.votes.include?(vote)
  end

end
