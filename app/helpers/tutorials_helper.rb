module TutorialsHelper

  def votes_display(votes)
     add = []
     for vote in votes
       add.push yield vote
     end
     add.reduce(:+) || 0
  end

  # same as above. just messing around.
  def display_votes(votes)
    add = []
    votes.each do |vote|
      add << vote.value
    end
    add.reduce(:+) || 0
  end

end
