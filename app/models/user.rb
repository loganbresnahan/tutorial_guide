class User < ApplicationRecord
  has_many :tutorials
  has_many :comments
  has_many :votes
  # has_many :tut_comments, foreign_key: 'tut_commenter_id', class_name: 'Comment'
  # has_many :com_comments, foreign_key: 'com_commenter_id', class_name: 'Comment'
  # has_many :tut_votes, foreign_key: 'tut_voter_id', class_name: 'Vote'
  # has_many :com_votes, foreign_key: 'com_voter_id', class_name: 'Vote'
  has_many :bookmarks

  validates :username, presence: true
  validates :email, presence: true, uniqueness: true
  has_secure_password

  validates :password, length: { minimum: 4 }

end
