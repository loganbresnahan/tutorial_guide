class Category < ApplicationRecord
  has_many :tutorial_categories
  has_many :tutorials, through: :tutorial_categories
end
