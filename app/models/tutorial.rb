require 'open-uri'

class Tutorial < ApplicationRecord
  validates :title, presence: true
  validates :link, presence: true, uniqueness: true
  validates :body, presence: true

  belongs_to :user
  has_many :comments
  has_many :votes
  has_many :tutorial_categories
  has_many :categories, through: :tutorial_categories
  has_many :bookmarks

  def nokogiri_doc(url)
    Nokogiri::HTML(open(url))
  end

  def title_parse(url)
    begin
      nokogiri_doc(url).title
    rescue StandardError
      "(Website title not available.)"
    end
  end

  def favicon_parse(url)
    # base_url = url.match(/(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/).to_s.chop
    # base_url = url.match(/(ftp|https|http)(:\/\/[a-z]*\.[a-z]*)/).to_s
    base_url = url.match(/(https|http)(:\/\/)([a-z]*)(\.)(.*)(\.)([a-z]*)(\/)/).to_s.chop
    favicon = "http://image.flaticon.com/icons/svg/164/164996.svg"

    begin
      nokogiri_doc(url).xpath("//head/link").each do |link|
        if link.attributes['href'].value.match(/favicon/)
          favicon = link.attributes['href'].value
        end
      end
    rescue StandardError
    end

    if favicon[0] == "/"
      favicon = base_url + favicon
    end
    return favicon
  end

  def votes_total(votes)
    add = []
    votes.each do |vote|
      add << vote.value
    end
    add.reduce(:+) || 0
  end

  def find_vote(tutorial, current_user)
    vote_return = nil
    tutorial.votes.each do |vote|
      current_user.votes.each do |personal_vote|
        vote_return = vote if vote.id == personal_vote.id
      end
    end
    vote_return
  end

  def find_vote_id(tutorial, current_user)
    tutorial.votes.each do |vote|
      current_user.votes.each do |personal_vote|
        return personal_vote.id if vote.id == personal_vote.id
      end
    end
  end

end
