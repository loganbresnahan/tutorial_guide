class Comment < ApplicationRecord
  belongs_to :user
  # belongs_to :tut_commenter, class_name: 'User', optional: true
  # belongs_to :com_commenter, class_name: 'User', optional: true
  belongs_to :tutorial
  has_many :child_comments, class_name: 'Comment', foreign_key: 'parent_comment_id'
  belongs_to :parent_comment, class_name: 'Comment', optional: true
  # has_many :child_comments, through: :comment_comments, source: :child_comment

  attr_accessor :info

  # def recursive_comments
  #   if self.child_comments[0].nil?
  #     #has to be here if there are no children to begin with
  #     return @info = {body: self.body,
  #                 name: self.tut_commenter.username,
  #                 vote_count: self.tut_commenter.tut_votes,
  #                 parent: self.parent_comment_id}
  #   end
  #   self.child_comments.each do |child|
  #     child.recursive_comments
  #   end
  #
  #   @info = {body: self.body,
  #        name: self.tut_commenter.username,
  #        vote_count: self.tut_commenter.tut_votes,
  #        parent: self.parent_comment_id}
  # end
  def make_class
    @class = CommentsPresenter.new
  end

  def first_comment
    @class.recursive_comments_display(recursive_comments)
  end

  def recursive_comments
    if self.child_comments[0].nil?
      @info = {body: self.body,
        name: self.tut_commenter.username,
        vote_count: self.tut_commenter.tut_votes,
        parent: self.parent_comment_id}
      return
      end

    self.child_comments.each do |child|

      @info = {body: self.body,
        name: self.tut_commenter.username,
        vote_count: self.tut_commenter.tut_votes,
        parent: self.parent_comment_id}

      child.recursive_comments
    end
  end

end
