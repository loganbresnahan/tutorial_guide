class TutorialCategory < ApplicationRecord
  belongs_to :tutorial
  belongs_to :category
end
