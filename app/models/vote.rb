class Vote < ApplicationRecord
  belongs_to :tutorial, optional: true
  belongs_to :comment, optional: true
  belongs_to :user
  # belongs_to :tut_voter, class_name: 'User', optional: true
  # belongs_to :com_voter, class_name: 'User', optional: true

  # validates :tut_voter_id, uniqueness: { scope: :tutorial_id, message: "you can only vote once on a tutorial" }
  # validates :com_voter_id, uniqueness: { scope: :comment_id, message: "you can only vote once on a comment" }
  validates :value, inclusion: { in: [1, -1] }
  validates :user_id, uniqueness: { scope: :tutorial_id }
  # validates :user_id, uniqueness: { scope: :comment_id }
end
