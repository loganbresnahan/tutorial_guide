class Bookmark < ApplicationRecord
  validates :link, presence: true
  belongs_to :user
  belongs_to :tutorial
end
